<!DOCTYPE html>
<html>
<title>KEPEGAWAIAN</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?=base_url()?>css/home.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}

.btn {
    background-color: DodgerBlue;
    border: none;
    color: white;
    padding: 2px 6px;
    font-size: 16px;
    cursor: pointer;
}

.btn:hover {
    background-color: RoyalBlue;
}

</style>
<body class="w3-light-grey">

<!-- Top container -->
<div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
  <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
  <span class="w3-bar-item w3-right">CUTI / IZIN</span>
</div>

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-top:43px;">
    <h2 style="margin-top: 5%;margin-left: 5%;margin-right: 5%; padding: 1%;color: white; background-color: green;">Detail Izin</h2>
<?php 

  $data = $heading['data'];
     foreach ($data as $key=>$topping) {
        $idijin = $topping['idijin']; 
        $nama_pegawai = $topping['nama']; 
        $tipe = $topping['tipe']; 
        $tgl_pengajuan = $topping['tgl_pengajuan']; 
        $mulai = $topping['mulai']; 
        $selesai = $topping['selesai']; 
        $keterangan = $topping['keterangan']; 
        $validasi_pengawas = $topping['validasi_pengawas']; 
        $validasi_admin = $topping['validasi_admin']; 
     }
 ?>

<div style="padding: 5%; margin-left: 5%;margin-right: 5%;background-color: white;">
    Jenis Pengajuan <br>

    <b><?php
       if ($tipe=='C') {
              echo 'CUTI';
            }else{
              echo 'IJIN';
            }
            ?></b>
    <hr>
    Nama Pegawai <br>

    <b><?php echo $nama_pegawai; ?></b>
    <hr>
    Tanggal Pengajuan <br>
    <b><?php echo $tgl_pengajuan; ?></b>
    <hr>
    Tanggal Mulai <br>
    <b><?php echo $mulai; ?></b>
    <hr>
    Tanggal Akhir <br>
    <b> <?php echo $selesai; ?></b>
    <hr>

    <form method="post" action="<?php echo base_url()."izin/validasi"; ?>">
          <input type="hidden" name="idijin" value=<?php echo $idijin ?> >
          <input type="hidden" name="role" value="admin" >

          <?php 
          if ($this->session->userdata('role')=='Pengawas') {
            if ($validasi_pengawas==0) {
                ?>
                <input type="hidden" name="type" value="valid" >
                <button type="submit" style="padding: 5px;" class="btn btn-default" >Setujui</button>  
           <?php
            }else{
                ?>
                <input type="hidden" name="type" value="cancel" >
                <button type="submit" style="padding: 5px; background-color: red;" class="btn btn-default" >Batalkan Persetujuan</button>  
                <?php
            }
          }else{
              if ($validasi_admin==0) {
                ?>
                <input type="hidden" name="type" value="valid" >
                <button type="submit" style="padding: 5px;" class="btn btn-default" >Setujui</button>  
           <?php
            }else{
                ?>
                <input type="hidden" name="type" value="cancel" >
                <button type="submit" style="padding: 5px; background-color: red;" class="btn btn-default" >Batalkan Persetujuan</button>  
                <?php
            }

          }
            
           ?>

        
    </form>
<br>
        <button type="button" style="background-color: red;padding: 5px;" class="btn btn-default" onclick="goBack()">Kembali</button>   


</div>

  <!-- End page content -->
</div>

<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidebar.style.display = 'block';
        overlayBg.style.display = "block";
    }
}

// Close the sidebar with the close button
function w3_close() {
    mySidebar.style.display = "none";
    overlayBg.style.display = "none";
}

function goBack() {
    window.history.back();
}
</script>

<script>

//   history.pushState(null, null, location.href);
//     window.onpopstate = function () {
//         history.go(1);
//     };

</script>

</body>
</html>
