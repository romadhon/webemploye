<!DOCTYPE html>
<html>
<title>KEPEGAWAIAN</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?=base_url()?>css/home.css">
<!--<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<body style="
    display: block;
    margin: 0px;
    margin-top: 0px;
    margin-right: 0px;
    margin-bottom: 0px;
    margin-left: 0px;
">

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>
<?php $this->view('master_menu_top'); ?>
<!-- !PAGE CONTENT! -->

<div class="w3-main" style="margin-left:30%;margin-right: 30%;margin-top:5%;">
<h2>Master Bagian</h2>
<hr>
   
<a style="background-color: green;color: white;padding: 5px;" class="btn btn-default" href='<?php echo base_url("Referensi/tambah_bagian"); ?>'>Tambah Data</a><br><br>

<table border="1" cellpadding="7">
  <tr style="height:50px;">
    <th>No</th>
    <th>bagian</th>
    <th colspan="2">Aksi</th>
  </tr>

  <?php
  if( ! empty($data)){ // Jika data siswa tidak sama dengan kosong, artinya jika data siswa ada
    foreach($data as $key=>$topping){
        ?>
        <tr style="height: 40px;">
        <td id="empTable" style="padding:10px;align:center;" >
         <?php echo $key+1 ?>  
         </td>
          <td style="padding:10px;align:center;">
         <?php echo $topping->nama_bagian; ?>  
         </td>
         <td><a style="background-color: blue;color: white;padding: 5px;" class="btn btn-default" href='<?php echo base_url("Referensi/ubah_bagian/".$topping->idbagian);?>'>Ubah</a></td>
          <td><a style="background-color: red;color: white;padding: 5px;" class="btn btn-default" href='<?php echo base_url("Referensi/hapus_bagian/".$topping->idbagian);?>'>Hapus</a></td>
         
      </tr>
      <?php
    }
  }else{ // Jika data siswa kosong
    echo "<tr><td align='center' colspan='7'>Data Tidak Ada</td></tr>";
  }
  ?>
</table>
  <!-- End page content -->
</div>

<script>

    function addField (argument) {
        var myTable = document.getElementById("myTable");
        var currentIndex = myTable.rows.length;
        var currentRow = myTable.insertRow(-1);

        var linksBox = document.createElement("Label");
        linksBox.setAttribute("name", "links" + currentIndex);
        linksBox.setAttribute("value", currentIndex);

        var keywordsBox = document.createElement("input");
        keywordsBox.setAttribute("name", "keywords" + currentIndex);

        var addRowBox = document.createElement("input");
        addRowBox.setAttribute("type", "button");
        addRowBox.setAttribute("value", "Simpan");
        addRowBox.setAttribute("onclick", "simpan();");
        addRowBox.setAttribute("class", "button");

        var currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(linksBox);

        currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(keywordsBox);

        currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(addRowBox);
    }

    function simpan (argument) {
        var action = "insert";
       var description = $("input[name='keywords8']").val();


        $.ajax({
           url: '/webemploye/referensi/data_bagian',
           type: 'POST',
           data: {action: action, description: description},
           error: function() {
              alert('Something is wrong');
           },
           success: function(data) {
            alert(data);
           }
        });
    }

  history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };

</script>

</body>
</html>
