<!DOCTYPE html>
<html>
<title>KEPEGAWAIAN</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?=base_url()?>css/home.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<body class="w3-light-grey">

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>
<?php $this->view('master_menu_top'); 

function tgl_indo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

?>
<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:20px;margin-right: 20px;margin-top:10px;">

<table >
  <tr style="height: 40px;">
    <th style="text-align: center;" >No</th>
    <th style="text-align: center;" >Pegawai</th>
    <th style="text-align: center;" >Jenis Pengajuan</th>
    <th style="text-align: center;" >Tanggal Pengajuan</th>
    <th style="text-align: center;" >Keterangan</th>
    <th style="text-align: center;" >Validasi Pengawas</th>
    <th style="text-align: center;" >Validasi Admin</th>
    <th style="text-align: center;" >Aksi</th>
  </tr>
  
   <?php 

  $data = $heading['data'];

 foreach ($data as $key=>$topping) {?>
 <tr style="height: 40px;">
   <td id="empTable" style="width: 5%;">
    <?php echo $key+1 ?>  
    </td>
     <td style="width: 15%;" >
    <?php echo $topping['idpegawai']; ?>  
    </td>
     <td style="width: 10%;">
    <?php 
      if ($topping['tipe']=='C') {
        echo 'CUTI';
      }else{
        echo 'IJIN';
      }

     ?>  
    </td>
     <td align="center" style="width: 13%;">
    <?php echo tgl_indo($topping['tgl_pengajuan']) ; ?>  
    </td>
    <td style="width: 17%;">
    <?php echo $topping['keterangan']; ?>  
    </td>
    
    <?php 
     $pStatus = $topping['validasi_pengawas']; 
     if ($pStatus==0) {
       ?>
      <td style=" background-color:red; color:white; width: 15%;">
       <?php
       echo "Belum Disetujui";

       ?>
      </td>
       <?php
     }else{
      ?>
      <td style="background-color:green; color:white; width: 15%;">
       <?php
       echo "Sudah Disetujui";

       ?>
      </td>
       <?php
     }
    ?>  
    
    <?php 
    $aStatus = $topping['validasi_admin']; 
     if ($aStatus==0) {
      ?>
      <td style=" background-color:red; color:white; width: 15%;">
       <?php
       echo "Belum Disetujui";

       ?>
      </td>
       <?php
     }else{
      ?>
      <td style="background-color:green; color:white; width: 15%;">
       <?php
       echo "Sudah Disetujui";

       ?>
      </td>
       <?php
     }
     ?>  
  
    <td>
      <?php echo anchor('izin/detailPerizinan/'.$topping['idijin'],'Detail'); ?>
    </td>
    
 </tr>
  
  <?php
    }
  ?>  
  
</table>
  <!-- End page content -->
</div>

<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidebar.style.display = 'block';
        overlayBg.style.display = "block";
    }
}

// Close the sidebar with the close button
function w3_close() {
    mySidebar.style.display = "none";
    overlayBg.style.display = "none";
}
</script>

<script>

  history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };

</script>

</body>
</html>
