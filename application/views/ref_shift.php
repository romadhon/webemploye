<!DOCTYPE html>
<html>
<title>KEPEGAWAIAN</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?=base_url()?>css/home.css">
<!--<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<body class="w3-light-grey">

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>
<?php $this->view('master_menu_top'); ?>
<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:30%;margin-right: 30%;margin-top:10%;">
    <form method="POST" action="<?php echo base_url() ?>index.php/dashboard/home<?php echo $id ?>">
          <button type="submit" id="btnList" style="background-color: green;color: white;padding: 5px;" class="btn btn-default">Tambah</button>   
        </form>
<table style="width:100%">

  <tr style="background-color: blue;color: white;height: 40px; align-items: center">
      <th style="align: center">No</th>
    <th>Shift</th>
    <th>Aksi</th>
  </tr>
  
   <?php 

  if ($data == NULL) {
      ?>
  <tr style="height: 40px;">
      <td colspan="3"> Tidak ada hasil</td>
  </tr>
  <?php
  } else {
//   var_dump(count($data));die;
 foreach ($data as $key=>$topping) {?>
 <tr style="height: 40px;">
   <td id="empTable">
    <?php echo $key+1 ?>  
    </td>
     <td>
    <?php echo $topping->nama; ?>  
    </td>
    <td>
      fd
    </td>
    
 </tr>
  
  <?php
    }
}
  ?>  
  
</table>
  <!-- End page content -->
</div>

<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidebar.style.display = 'block';
        overlayBg.style.display = "block";
    }
}

// Close the sidebar with the close button
function w3_close() {
    mySidebar.style.display = "none";
    overlayBg.style.display = "none";
}
</script>

<script>

  history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };

</script>

</body>
</html>
