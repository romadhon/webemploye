<!DOCTYPE html>
<html>
<title>W3.CSS Template</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css" rel="stylesheet"/>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet"/>
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<body class="w3-light-grey">

<!-- Top container -->
<div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
  <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
  <span class="w3-bar-item w3-right">KEPEGAWAIAN</span>
</div>

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br>
  
  <div style="margin:10px;">
  <h3>Cari Tahun</h3>
    <form action="<?php echo base_url() ?>index.php/AbsensiController/karyawan/<?php echo $this->session->userdata('id_karyawan')?>/01" method="POST"> 
        <input style="width:50%;"  type="input" name="tahun" id="edit1"  size="4" maxlength="4" />
        <input class="btn btn-info" type="submit" value="Cari">
    </form>
  </div>
 
<hr>
  <div class="w3-bar-block"> 
  <h2 style="margin-left: 5%;">
  <?php echo $this->session->userdata('tahun'); ?>   
  </h2>
  Pilih Bulan :
    <form action="<?php echo base_url() ?>index.php/AbsensiController/karyawan/<?php echo $this->session->userdata('id_karyawan')?>/01" method="POST"> 
        <input type="hidden" name="tahun" id="edit1" value="<?php echo $this->session->userdata('tahun'); ?>" />
        <input style="width:100%;padding:2%;margin-top:1px;" class="btn btn-primary" type="submit" value="Januari">
    </form>

    <form action="<?php echo base_url() ?>index.php/AbsensiController/karyawan/<?php echo $this->session->userdata('id_karyawan')?>/02" method="POST"> 
        <input type="hidden" name="tahun" id="edit1" value="<?php echo $this->session->userdata('tahun'); ?>" />
        <input style="width:100%;padding:2%;margin-top:1px;" class="btn btn-primary" type="submit" value="Pebruari">
    </form>

    <form action="<?php echo base_url() ?>index.php/AbsensiController/karyawan/<?php echo $this->session->userdata('id_karyawan')?>/03" method="POST"> 
        <input type="hidden" name="tahun" id="edit1" value="<?php echo $this->session->userdata('tahun'); ?>" />
        <input style="width:100%;padding:2%;margin-top:1px;" class="btn btn-primary" type="submit" value="Maret">
    </form>

    <form action="<?php echo base_url() ?>index.php/AbsensiController/karyawan/<?php echo $this->session->userdata('id_karyawan')?>/04" method="POST"> 
        <input type="hidden" name="tahun" id="edit1" value="<?php echo $this->session->userdata('tahun'); ?>" />
        <input style="width:100%;padding:2%;margin-top:1px;" class="btn btn-primary" type="submit" value="April">
    </form>

    <form action="<?php echo base_url() ?>index.php/AbsensiController/karyawan/<?php echo $this->session->userdata('id_karyawan')?>/05" method="POST"> 
        <input type="hidden" name="tahun" id="edit1" value="<?php echo $this->session->userdata('tahun'); ?>" />
        <input style="width:100%;padding:2%;margin-top:1px;" class="btn btn-primary" type="submit" value="Mei">
    </form>

    <form action="<?php echo base_url() ?>index.php/AbsensiController/karyawan/<?php echo $this->session->userdata('id_karyawan')?>/06" method="POST"> 
        <input type="hidden" name="tahun" id="edit1" value="<?php echo $this->session->userdata('tahun'); ?>" />
        <input style="width:100%;padding:2%;margin-top:1px;" class="btn btn-primary" type="submit" value="Juni">
    </form>

    <form action="<?php echo base_url() ?>index.php/AbsensiController/karyawan/<?php echo $this->session->userdata('id_karyawan')?>/07" method="POST"> 
        <input type="hidden" name="tahun" id="edit1" value="<?php echo $this->session->userdata('tahun'); ?>" />
        <input style="width:100%;padding:2%;margin-top:1px;" class="btn btn-primary" type="submit" value="Juli">
    </form>

    <form action="<?php echo base_url() ?>index.php/AbsensiController/karyawan/<?php echo $this->session->userdata('id_karyawan')?>/08" method="POST"> 
        <input type="hidden" name="tahun" id="edit1" value="<?php echo $this->session->userdata('tahun'); ?>" />
        <input style="width:100%;padding:2%;margin-top:1px;" class="btn btn-primary" type="submit" value="Agustus">
    </form>

    <form action="<?php echo base_url() ?>index.php/AbsensiController/karyawan/<?php echo $this->session->userdata('id_karyawan')?>/09" method="POST"> 
        <input type="hidden" name="tahun" id="edit1" value="<?php echo $this->session->userdata('tahun'); ?>" />
        <input style="width:100%;padding:2%;margin-top:1px;" class="btn btn-primary" type="submit" value="September">
    </form>

    <form action="<?php echo base_url() ?>index.php/AbsensiController/karyawan/<?php echo $this->session->userdata('id_karyawan')?>/10" method="POST"> 
        <input type="hidden" name="tahun" id="edit1" value="<?php echo $this->session->userdata('tahun'); ?>" />
        <input style="width:100%;padding:2%;margin-top:1px;" class="btn btn-primary" type="submit" value="Oktober">
    </form>

    <form action="<?php echo base_url() ?>index.php/AbsensiController/karyawan/<?php echo $this->session->userdata('id_karyawan')?>/11" method="POST"> 
        <input type="hidden" name="tahun" id="edit1" value="<?php echo $this->session->userdata('tahun'); ?>" />
        <input style="width:100%;padding:2%;margin-top:1px;" class="btn btn-primary" type="submit" value="Nopember">
    </form>

    <form action="<?php echo base_url() ?>index.php/AbsensiController/karyawan/<?php echo $this->session->userdata('id_karyawan')?>/12" method="POST"> 
        <input type="hidden" name="tahun" id="edit1" value="<?php echo $this->session->userdata('tahun'); ?>" />
        <input style="width:100%;padding:2%;margin-top:1px;" class="btn btn-primary" type="submit" value="Desember">
    </form>
    <br><br>
  </div>
</nav>

<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidebar.style.display = 'block';
        overlayBg.style.display = "block";
    }
}

// Close the sidebar with the close button
function w3_close() {
    mySidebar.style.display = "none";
    overlayBg.style.display = "none";
}

function buttonActive(element) {
    $(element).toggleClass('w3-bar-item w3-button w3-padding w3-blue');
}


</script>

<script type="text/javascript">
       $(document).ready(function(){
    $('[id^=edit]').keypress(validateNumber);
});

function validateNumber(event) {
    var key = window.event ? event.keyCode : event.which;
    if (event.keyCode === 8 || event.keyCode === 46) {
        return true;
    } else if ( key < 48 || key > 57 ) {
        return false;
    } else {
    	return true;
    }
};
    </script>

</body>
</html>
