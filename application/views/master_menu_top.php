<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<style>
  .navbar-default {
    background-color: #0c6311;
    border-color: #4a1111;
    color: #f5f5f5;
}
.navbar-default .navbar-nav>li>a {
    color: #f5f5f5;
}
.navbar-default .navbar-brand {
    color: #f5f5f5;
}

.navbar-default .navbar-nav>li>a:focus, .navbar-default .navbar-nav>li>a:hover {
    color: #bad010;
    background-color: transparent;
}
</style>
</head>
<body>

<nav class="navbar navbar-default">
  <div class="container-fluid">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo base_url() ?>dashboard/home">Kepegawaian</a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <ul class="nav navbar-nav">
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Referensi <span class="caret"></span></a>
          <ul class="dropdown-menu">
          <li><a href="<?php echo base_url() ?>index.php/referensi/data_bagian">Bagian</a></li>
          <li><a href="<?php echo base_url() ?>index.php/karyawan/tambah">Tambah Karyawan</a></li>
          </ul>
        </li>
        <li><a href="<?php echo base_url() ?>AbsensiController/upload_absensi">Unggah Absensi</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Data <span class="caret"></span></a>
          <ul class="dropdown-menu">
          <li><a href="<?php echo base_url() ?>izin/index">Cuti/Izin</a></li>
             <li><a href="<?php echo base_url() ?>karyawan/data_karyawan">Data Karyawan</a></li>
             <li><a href="<?php echo base_url() ?>AbsensiController/getEmployee">Presensi</a></li>
          </ul>
        </li>
      </ul>
      <!-- <form class="navbar-form navbar-left">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Search">
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
      </form> -->
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hai <?php echo $this->session->userdata('role'); echo ' '; echo $this->session->userdata('nama');?> <span class="caret"></span></a>
          <ul class="dropdown-menu">
  
            <li><a href="<?php echo base_url() ?>dashboard/logout">Keluar</a></li>
          </ul>
        </li>
      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>



  <!-- jQuery CDN -->
         <script src="https://code.jquery.com/jquery-1.12.0.min.js"></script>
         <!-- Bootstrap Js CDN -->
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</body>
</html>
