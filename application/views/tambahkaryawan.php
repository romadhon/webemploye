<!DOCTYPE html>
<html>
<title>KEPEGAWAIAN</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?=base_url()?>css/home.css">
<!--<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<body style="
    display: block;
    margin: 0px;
    margin-top: 0px;
    margin-right: 0px;
    margin-bottom: 0px;
    margin-left: 0px;
">

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>
<?php $this->view('master_menu_top'); ?>
<!-- !PAGE CONTENT! -->

<div class="w3-main" style="margin-left:30%;margin-right: 30%;margin-top:5%;">
<h1>Tambah Karyawan </h1>
  <br>

  <form method="POST" action="<?php echo base_url() ?>index.php/karyawan/tambah">

    <table>
   
    <tr style="background-color: white; height: 50px;">
      <td style="padding-left: 2%;"><b>Nama</b></td>
      <td style="padding-left: 2%;">
                    <input type="text" required data-errormessage-value-missing="Harus diisi" class="form-control" name="nama" value="<?php echo $nama_pegawai ?>">
       </td>
    </tr>

    <tr style="background-color: white; height: 50px;">
      <td style="padding-left: 2%;"><b>Bagian</b></td>
      <td style="padding-left: 2%;">
          <?php
          foreach ($ref_region as $ref_reg){
            ?>
            <input type="radio" name="bagian" value="<?php echo $ref_reg->nama_bagian ?>" required data-errormessage-value-missing="Silakan pilih salah satu" > <?php echo $ref_reg->nama_bagian ?> <br> 
           
            
            <?php
          }
          ?>
      </td>
    </tr>
    <tr style="background-color: white; height: 50px;">
      <td style="padding-left: 2%;"><b>Jabatan</b></td>
      <td style="padding-left: 2%;">
        <?php
        foreach ($ref_positions as $ref_pos) {
          ?>
            <input type="radio" name="jabatan" value="<?php echo $ref_pos->nama ?>" required data-errormessage-value-missing="Silakan pilih salah satu" > <?php echo $ref_pos->nama ?> <br> 
                      
          <?php
        }
        ?>
      </td>
      
    </tr>
    <tr style="background-color: white; height: 50px;">
      <td style="padding-left: 2%;"><b>Kelompok</b></td>
      <td style="padding-left: 2%;">
        <input type="radio" name="kelompok" value="1" required data-errormessage-value-missing="Silakan pilih salah satu" checked> Kelompok 1 
        <input type="radio" name="kelompok" value="2"> Kelompok 2 
        <input type="radio" name="kelompok" value="3"> Kelompok 3 
       
        
      </td>
      
    </tr>

    <tr style="background-color: white; height: 50px;">
      <td style="padding-left: 2%;"><b>Jenis Kelamin</b></td>
      <td style="padding-left: 2%;">
        <form action="">
        <input type="radio" name="jenis_kelamin" value="L" required data-errormessage-value-missing="Silakan pilih salah satu" checked> Pria 
        <input type="radio" name="jenis_kelamin" value="P"> Wanita
        </form>
      </td>
      
    </tr>

    <tr style="background-color: white; height: 50px;">
      <td style="padding-left: 2%;"><b>Alamat</b></td>
      <td style="padding-left: 2%;"> <input type="text" required data-errormessage-value-missing="Harus diisi" class="form-control" name="alamat" value=""></td>
    </tr>

    <!-- <tr style="background-color: white; height: 50px;">
      <td style="padding-left: 2%;"><b>Password</b></td>
      <td style="padding-left: 2%;"> <input type="text" class="form-control" name="pass" value="<?php echo $pass?>"></td>
    </tr> -->
  </table>
<br>
<input type="hidden" class="form-control" name="action" value="add">
          <button type="submit" id="btnList" style="background-color: blue;color: white;padding: 5px;" class="btn btn-default">Simpan</button>             
        </form>
<br><br>
        <!-- <form method="POST" action="<?php echo base_url() ?>index.php/dashboard/home<?php echo $id ?>">
          <button type="submit" id="btnList" style="background-color: red;color: white;padding: 5px;" class="btn btn-default">Kembali</button>   
        </form> -->

  <!-- End page content -->
</div>

<script>

    function addField (argument) {
        var myTable = document.getElementById("myTable");
        var currentIndex = myTable.rows.length;
        var currentRow = myTable.insertRow(-1);

        var linksBox = document.createElement("Label");
        linksBox.setAttribute("name", "links" + currentIndex);
        linksBox.setAttribute("value", currentIndex);

        var keywordsBox = document.createElement("input");
        keywordsBox.setAttribute("name", "keywords" + currentIndex);

        var addRowBox = document.createElement("input");
        addRowBox.setAttribute("type", "button");
        addRowBox.setAttribute("value", "Simpan");
        addRowBox.setAttribute("onclick", "simpan();");
        addRowBox.setAttribute("class", "button");

        var currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(linksBox);

        currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(keywordsBox);

        currentCell = currentRow.insertCell(-1);
        currentCell.appendChild(addRowBox);
    }

    function simpan (argument) {
        var action = "insert";
       var description = $("input[name='keywords8']").val();


        $.ajax({
           url: '/webemploye/referensi/data_bagian',
           type: 'POST',
           data: {action: action, description: description},
           error: function() {
              alert('Something is wrong');
           },
           success: function(data) {
            alert(data);
           }
        });
    }

  history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };

</script>

</body>
</html>
