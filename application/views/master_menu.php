<!DOCTYPE html>
<html>
<title>W3.CSS Template</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<script src="https://code.jquery.com/jquery-1.10.2.js"></script>
<body class="w3-light-grey">

<!-- Top container -->
<div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
  <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
  <span class="w3-bar-item w3-right">KEPEGAWAIAN</span>
</div>

<!-- Sidebar/menu -->
<nav class="w3-sidebar w3-collapse w3-white w3-animate-left" style="z-index:3;width:300px;" id="mySidebar"><br>
  <div class="w3-container w3-row">
    <div class="w3-col s4">
      <img src="http://localhost/webemploye/image/ic_admin.png" class="w3-circle w3-margin-right" style="width:46px">
    </div>
    <div class="w3-col s8 w3-bar">
      <span>Welcome, <strong> <?php echo $this->session->userdata('nama') ?> </strong></span><br>
       <span><strong> <?php 
        if ($role=='A') {
          echo "ADMIN";
        }else{
          echo "PENGAWAS";
        }

       ?> </strong></span><br>
    </div>
  </div>
  <hr>
  <div class="w3-container">
   
    <a href="<?php echo base_url() ?>dashboard/home" class="w3-bar-item w3-button w3-padding"> <h5>Dashboard</h5></a>
  </div>
  <div class="w3-bar-block">
    <a href="#" class="w3-bar-item w3-button w3-padding-16 w3-hide-large w3-dark-grey w3-hover-black" onclick="w3_close()" title="close menu"><i class="fa fa-remove fa-fw"></i>  Close Menu</a>
    <a href="<?php echo base_url() ?>AbsensiController/upload_absensi" class="w3-bar-item w3-button w3-padding"><i class="fa fa-upload fa-fw"></i> Upload Presensi</a>
    <a href="<?php echo base_url() ?>AbsensiController/getEmployee" class="w3-bar-item w3-button w3-padding"><i class="fa fa-users fa-fw"></i>  Presensi</a>
    <a href="<?php echo base_url() ?>izin/index" class="w3-bar-item w3-button w3-padding" ><i class="fa fa-eye fa-fw"></i>  Cuti/Izin</a>
    <a href="<?php echo base_url() ?>karyawan/data_karyawan" class="w3-bar-item w3-button w3-padding"><i class="fa fa-user fa-fw"></i>  Karyawan</a>
    <a href="<?php echo base_url() ?>dashboard/logout" class="w3-bar-item w3-button w3-padding"><i class="fa fa-sign-out fa-fw"></i>  Keluar</a><br><br>
  </div>
</nav>


<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidebar.style.display = 'block';
        overlayBg.style.display = "block";
    }
}

// Close the sidebar with the close button
function w3_close() {
    mySidebar.style.display = "none";
    overlayBg.style.display = "none";
}

function buttonActive(element) {
    $(element).toggleClass('w3-bar-item w3-button w3-padding w3-blue');
}
</script>

</body>
</html>
