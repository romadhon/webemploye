<!DOCTYPE html>
<html>
<title>KEPEGAWAIAN</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?=base_url()?>css/home.css">
<link rel="stylesheet" href="<?=base_url()?>css/w3css.css">
<link rel="stylesheet" href="<?=base_url()?>css/googleapis.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
.dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 10px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}


.btn {
    background-color: DodgerBlue;
    border: none;
    color: white;
    padding: 2px 6px;
    font-size: 16px;
    cursor: pointer;
}

.btn:hover {
    background-color: RoyalBlue;
}

</style>
<body class="w3-light-grey">

<!-- Top container -->
<div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
  <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
  <span class="w3-bar-item w3-right" style="padding-right: 5%;">Karyawan</span>
</div>
<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:10px;margin-top:43px;">
<br>
<?php 
    foreach ($data as $key => $value) {
      $namaBagian = $value->nama_bagian;
      $idBagian = $value->idbagian;
    }
 ?>

<div style="padding: 5%; margin-left: 5%;margin-right: 5%;background-color: white;">
<h1>Tambah Bagian </h1>
 
 <br>
 <br>

  <form method="POST" action="<?php echo base_url() ?>index.php/Referensi/ubah_bagian">

    <table>
   
    <tr style="background-color: white; height: 50px;">
      <td style="padding-left: 2%;"><b>Nama Bagian</b></td>
      <td style="padding-left: 2%;">
                    <input type="text" required data-errormessage-value-missing="Harus diisi" class="form-control" name="nama" value="<?php echo $namaBagian?>">
       </td>
    </tr>
  </table>
<br>
<input type="hidden" class="form-control" name="action" value="<?php echo ACT_EDIT ?>">
<input type="hidden" class="form-control" name="idbagian" value="<?php echo $idBagian ?>">
          <button type="submit" id="btnList" style="background-color: blue;padding: 5px;" class="btn btn-default">Simpan</button>             
        </form>
<br>
        <form method="POST" action="<?php echo base_url() ?>index.php/dashboard/home<?php echo $id ?>">
          <button type="submit" id="btnList" style="background-color: green;padding: 5px;" class="btn btn-default">Kembali</button>   
        </form>

</div>

  <!-- End page content -->
</div>

<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidebar.style.display = 'block';
        overlayBg.style.display = "block";
    }
}

// Close the sidebar with the close button
function w3_close() {
    mySidebar.style.display = "none";
    overlayBg.style.display = "none";
}

function goBack() {
    window.history.back();
}


</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script> 
<script>

  history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };

</script>

</body>
</html>
