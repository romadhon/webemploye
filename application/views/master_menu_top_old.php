<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<nav class="navbar navbar-inverse">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="<?php echo base_url() ?>dashboard/home">Kepegawaian</a>
    </div>
    <ul class="nav navbar-nav">
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Referensi <span class="caret"></span></a>
        <ul class="dropdown-menu">
          <!-- <li><a href="<?php echo base_url() ?>index.php/referensi/data_shift">Shift</a></li> -->
          <li><a href="<?php echo base_url() ?>index.php/referensi/data_bagian">Bagian</a></li>
          <!-- <li><a href="#">Jabatan</a></li> -->
          <li><a href="<?php echo base_url() ?>index.php/karyawan/tambah">Tambah Karyawan</a></li>
        </ul>
      </li>
      <li><a href="<?php echo base_url() ?>AbsensiController/upload_absensi">Unggah Absensi</a></li>
      <li class="dropdown"><a class="dropdown-toggle" data-toggle="dropdown" href="#">Data <span class="caret"></span></a>
        <ul class="dropdown-menu">
             <li><a href="<?php echo base_url() ?>izin/index">Cuti/Izin</a></li>
             <li><a href="<?php echo base_url() ?>karyawan/data_karyawan">Data Karyawan</a></li>
             <li><a href="<?php echo base_url() ?>AbsensiController/getEmployee">Presensi</a></li>
          
        </ul>
      </li>
     
      <li><a href="<?php echo base_url() ?>dashboard/logout">Keluar</a></li>
    </ul>

    <div class=".navbar-right">
        <ul >
            <li class="nav-item">
                <a class="nav-link" href="#">Right</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Link</a>
            </li>
        </ul>
    </div>
    
  </div>
</nav>
 
</body>
</html>
