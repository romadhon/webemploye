<!DOCTYPE html>
<html>
<title>KEPEGAWAIAN</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?=base_url()?>css/home.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
.dropbtn {
    background-color: #4CAF50;
    color: white;
    padding: 10px;
    font-size: 16px;
    border: none;
    cursor: pointer;
}


.btn {
    background-color: DodgerBlue;
    border: none;
    color: white;
    padding: 2px 6px;
    font-size: 16px;
    cursor: pointer;
}

.btn:hover {
    background-color: RoyalBlue;
}

</style>
<body class="w3-light-grey">

<!-- Top container -->
<div class="w3-bar w3-top w3-black w3-large" style="z-index:4">
  <button class="w3-bar-item w3-button w3-hide-large w3-hover-none w3-hover-text-light-grey" onclick="w3_open();"><i class="fa fa-bars"></i>  Menu</button>
  <span class="w3-bar-item w3-right" style="padding-right: 5%;">PRESENSI</span>
</div>

<?php $this->view('master_menu_month'); ?>
<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>

<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:300px;margin-top:43px;">
<br>

<?php 

function tgl_indo($tanggal){
	$bulan = array (
		1 =>   'Januari',
		'Februari',
		'Maret',
		'April',
		'Mei',
		'Juni',
		'Juli',
		'Agustus',
		'September',
		'Oktober',
		'November',
		'Desember'
	);
	$pecahkan = explode('-', $tanggal);
 
	return $pecahkan[2] . ' ' . $bulan[ (int)$pecahkan[1] ] . ' ' . $pecahkan[0];
}

    $hasmap = array();
     foreach ($pegawai as $h) {
        $id_pegawai = $h->id; 
        $nama_pegawai = $h->nama; 
        $bagian = $h->role; 
        $kelompok = $h->kelompok;
     }
 ?>

<div style="padding: 5%; margin-left: 5%;margin-right: 5%;background-color: white;">

  <form class="form-signin" method="POST" action="<?php echo base_url() ?>index.php/absensiController/getEmployee">
    <button type="submit" id="btnList" style="background-color: red;color: white;padding: 5px;" class="btn btn-default">Kembali Ke Daftar</button>   
          
  </form>

 
 <br>
 <br>
  <table>
    <tr style="background-color: white; height: 30px;">
      <td style="padding-left: 2%;"> <b>ID Pegawai</b> </td>
      <td style="padding-left: 2%;"> <?php echo $id_pegawai?> </td>
    </tr>
    
    <tr style="background-color: white; height: 30px;">
      <td style="padding-left: 2%;"><b>Nama</b></td>
      <td style="padding-left: 2%;"><?php echo $nama_pegawai?></td>
    </tr>

    <tr style="background-color: white; height: 30px;">
      <td style="padding-left: 2%;"><b>Bagian</b></td>
      <td style="padding-left: 2%;"><?php echo $bagian?></td>
    </tr>

  </table>

<h1>
  <?php 
   $bulan = json_decode($date_dimension[0]);
      foreach ($date_dimension as $key => $value) { 
      if($key==0){
        if ($value->month == 1) {
            echo 'Januari';
        } elseif ($value->month == 2) {
          echo 'Pebruari';
        } elseif ($value->month == 3) {
          echo 'Maret';
        } elseif ($value->month == 4) {
          echo 'April';
        } elseif ($value->month == 5) {
          echo 'Mei';
        } elseif ($value->month == 6) {
          echo 'Juni';
        } elseif ($value->month == 7) {
          echo 'Juli';
        } elseif ($value->month == 8) {
          echo 'Agustus';
        } elseif ($value->month == 9) {
          echo 'September';
        } elseif ($value->month == 10) {
          echo 'Oktober';
        } elseif ($value->month == 11) {
          echo 'Nopember';
        } elseif ($value->month == 12) {
          echo 'Desember';
        }
    }
}
  
?>
</h1>
<hr>
       <br>
       <table class="table table-striped">
           <tr style="height: 35px">
              <th style="width: 3%; text-align: center;">No</th>
              <th style="text-align: center;">Tanggal</th>
              <!-- <th style="text-align: center;">Hari</th> -->
              <th style="text-align: center;">Shift</th>
              <th style="text-align: center;">Waktu 1</th>
              <th style="text-align: center;">Waktu 2</th>
              <th style="text-align: center;">Waktu 3</th>
              <th style="text-align: center;">Waktu 4</th>
              <th style="text-align: center;">Kode</th>
              <th style="text-align: center;">Lembur</th>
            </tr>

  <?php 
   $no=1;
   
 foreach ($presensi as $key => $value) {?>
 <tr style="height: 35px">
   <td><?php echo $key+1 ?></td>
   <td><?php echo tgl_indo($value['tanggal'])  ?></td>
   <td><?php echo $value['shift'] ?></td>
   <td><?php echo $value['masuk'] ?></td>
   <td><?php echo $value['istirahat1'] ?></td>
   <td><?php echo $value['istirahat2'] ?></td>
   <td><?php echo $value['pulang'] ?></td>
   <td><?php echo $value['status'] ?></td>
   <td><?php echo $value['lembur'] ?></td>
 </tr>
  
  <?php
    }
  ?>  

       </table>
</div>

  <!-- End page content -->
</div>

<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidebar.style.display = 'block';
        overlayBg.style.display = "block";
    }
}

// Close the sidebar with the close button
function w3_close() {
    mySidebar.style.display = "none";
    overlayBg.style.display = "none";
}

function goBack() {
    window.history.back();
}


</script>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js" type="text/javascript"></script> 
<script>

  history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };

</script>

</body>
</html>
