<!DOCTYPE html>
<html>
<title>KEPEGAWAIAN</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- <link rel="stylesheet" href="<?=base_url()?>css/home.css"> -->
<!--<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">-->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<body class="w3-light-grey">

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>
<?php $this->view('master_menu_top'); ?>
<!-- !PAGE CONTENT! -->

<div class="w3-main" style="    margin-left: 30%;
    margin-right: 30%;
    margin-top: 5%;
    width: 50%;
    padding-bottom: 100px;
    padding-top: 1%;
    text-align: -webkit-center;
    height: 50%;
    background: #d2d2e4;
    background-color: #a5c0d8;">
<h2>Unggah Absensi</h2>
<hr>
<form method="post" action="<?php echo base_url("index.php/AbsensiController/uploadTxt"); ?>" enctype="multipart/form-data">
    <input class="form-control" type="file" name="file">
    <input style="margin-top:2px;" class="btn btn-primary" type="submit" name="preview" value="Submit">
  </form>
  <br>
  <H2><?php echo $heading ?> </H2>

  <!-- End page content -->
</div>

<script>

    

  history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };

</script>

</body>
</html>

