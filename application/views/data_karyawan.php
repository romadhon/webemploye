<!DOCTYPE html>
<html>
<title>KEPEGAWAIAN</title>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="<?=base_url()?>css/home.css">
<link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
html,body,h1,h2,h3,h4,h5 {font-family: "Raleway", sans-serif}
</style>
<body class="w3-light-grey">

<!-- Overlay effect when opening sidebar on small screens -->
<div class="w3-overlay w3-hide-large w3-animate-opacity" onclick="w3_close()" style="cursor:pointer" title="close side menu" id="myOverlay"></div>
<?php $this->view('master_menu_top'); ?>
<!-- !PAGE CONTENT! -->
<div class="w3-main" style="margin-left:20px;margin-right: 20px;margin-top:10px;">

<div style="padding: 1%; background: #009fff;">
  <h2 for="male" style="color: #FFFFFF"><b>Data Karyawan</b></h2> 

  <form method="post" action="<?php echo base_url("index.php/Karyawan/search"); ?>" enctype="multipart/form-data">
   
       <input type="nama" name="nama"  placeholder="Nama">
   
      <input type="submit" name="cari" value="Cari">
  </form>
  <hr>
</div>




<table>
    <tr >
    <th style="width: 60px;height: 40px;">No</th>
    <th>Nama</th>
    <th>Bagian</th>
    <th>Aksi</th>
  </tr>
  
   <?php 
   $no = $this->uri->segment(3)+1;
 foreach ($heading as $topping) {?>
 <tr>
   <td style="width: 40px;height: 40px;">
    <?php echo $no++; ?>  
    </td>
     <td>
    <?php echo $topping->nama; ?>  
    </td>
     <td>
    <?php echo $topping->role; ?>  
    </td>
   <td>
   <?php 
   echo anchor('karyawan/detail/'.$topping->id,'Detail'); ?> 
    </td>
 </tr>
  
  <?php
    }
  ?>  
</table>
<?php 
	echo $this->pagination->create_links();
	?>
  <!-- End page content -->
</div>

<script>
// Get the Sidebar
var mySidebar = document.getElementById("mySidebar");

// Get the DIV with overlay effect
var overlayBg = document.getElementById("myOverlay");

// Toggle between showing and hiding the sidebar, and add overlay effect
function w3_open() {
    if (mySidebar.style.display === 'block') {
        mySidebar.style.display = 'none';
        overlayBg.style.display = "none";
    } else {
        mySidebar.style.display = 'block';
        overlayBg.style.display = "block";
    }
}

// Close the sidebar with the close button
function w3_close() {
    mySidebar.style.display = "none";
    overlayBg.style.display = "none";
}
</script>

<script>

  history.pushState(null, null, location.href);
    window.onpopstate = function () {
        history.go(1);
    };

</script>

</body>
</html>
