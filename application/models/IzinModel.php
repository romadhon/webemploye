<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class IzinModel extends CI_Model {

  public function getAll()
  {
      $data = $this->db->get('ijin')->result_array();

      return array('status' => 200 , 'data' => $data);
  }

public function getDataIzinPeridijin($id)
{
  $res = $this->db->query("SELECT 
  ijin.idijin, 
  user.nama , 
  ijin.tipe ,
  ijin.tgl_pengajuan, 
  ijin.mulai, ijin.selesai, 
  ijin.selesai, 
  ijin.keterangan, 
  ijin.validasi_pengawas, 
  ijin.validasi_admin FROM ijin
   JOIN user ON ijin.idpegawai= `user`.id WHERE ijin.idijin=".$id." ")->result_array();
  return array('data' => $res);  
}

public function getDataIzinPerid($id)
{ 
  $data = $this->db->get_where('ijin', array('idpegawai'=>$id))->result_array();
  return array('status' => 200 , 'data' => $data);  
}

  public function ajukan($id, $tglPengajuan,$mulai,$selesai,$keterangan,$tipe){

    $this->db->trans_start();
    if ($tipe == 'I') {
       $data = array('idpegawai' => $id, 'tipe' => 'I', 'tgl_pengajuan' => $tglPengajuan, 'mulai' => $mulai, 'selesai' => $selesai, 'keterangan' => $keterangan );
    }else{
       $data = array('idpegawai' => $id, 'tipe' => 'C', 'tgl_pengajuan' => $tglPengajuan, 'mulai' => $mulai, 'selesai' => $selesai, 'keterangan' => $keterangan );
    }
    $this->db->insert('ijin', $data);
       
        if ($this->db->trans_status() === FALSE) {
          $this->db->trans_rollback();
          return array('status' => 500 , 'message' => 'Internal Server Error' );
        }else{
          $this->db->trans_commit();
          return array('status' => 200 , 'message' => 'Berhasil Diajukan.');
        }

  }
  
  public function validasi($role,$type,$idijin)
  {
    if ($role=='Pengawas') {
      if ($type=='valid') {
        $data = array(
        'validasi_pengawas' => '1'
        );
      }else{
        $data = array(
        'validasi_pengawas' => '0'
        );
      }
      
    }else{
      if ($type=='valid') {
         $data = array(
        'validasi_admin' => '1'
        );
      }else{
         $data = array(
        'validasi_admin' => '0'
        );
       }        
    }

      $this->db->trans_start();
      $this->db->where('idijin', $idijin);
      $this->db->update('ijin', $data);

       if ($this->db->trans_status() === FALSE) {
          $this->db->trans_rollback();
          return array('status' => 500 , 'message' => 'Internal Server Error' );
        }else{
          $this->db->trans_commit();
          return array('status' => 200 , 'message' => 'Berhasil Divalidasi.');
        }

  }

}