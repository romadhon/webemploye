<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class LocationModel extends CI_Model {
		public function getAllLocation(){
			$this->db->select('nama,alamat,email,hp,role,longitute,latitute');
			$this->db->from('users');
			$res = $this->db->get()->result();

			if (count($res)==0) {
				return array('status' => 201 , 'message' => 'Lokasi bengkel tidak ditemukan.' );	
			}else{
				return array('status' => 200 , 'data' => $res);	
			}		
		}
	}
?>