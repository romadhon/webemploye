<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class UserModel extends CI_Model {

		public function getEmployee()
		{
			$res = $this->db->query("SELECT id,id_karyawan,nama,role FROM user")->result();
			return $res;
		}

		function data($number,$offset){
			return $query = $this->db->get('user',$number,$offset)->result();		
		}
	 
		function jumlah_data(){
			return $this->db->get('user')->num_rows();
		}

		public function getEmployeePerId($id)
		{
			$res = $this->db->get_where('user',array('id'=>$id))->result();
			return $res;
		}

		public function login_web($user, $pass)
		{
		

			$last_login = date('Y-m-d H:i:s');
			$token = md5(rand());
			$expired_at = date("Y-m-d H:i:s", strtotime('+12 hours'));
				
					$login = $this->db->get_where('users', array('email'=>$user, 'password'=>$pass))->result_array();
					if (count($login)==0) {
						return array('status' => 205 , 'message' => 'Email atau Kata sandi tidak sesuai');
					}else{
						$this->db->insert('login',array('username'=>$user, 'lastlogin'=>$last_login));
						return array('status' => 200 , 'message' => 'Berhasil login','token' => $token, 'data' => $login);	
					}	
		}

		public function mobile_login($username, $password)
		{
			$user = $username;
			$pass = md5($password);

					$login = $this->db->get_where('user', array('id'=>$user, 'password'=>$pass))->result_array();
					if (count($login)==0) {
						return array('status' => 205 , 'message' => 'Kode atau Kata Sandi tidak sesuai','data' => $login);
					}else{
						return array('status' => 200 , 'message' => 'Berhasil login', 'data' => $login);
					}			
		}

		public function hapus($id)
		{
			$query = $this->db->delete('user', array('id' => $id)); 
			return array('status' => 200 , 'message' => 'Berhasil menghapus karyawan');
		}

		public function ubah($data,$id)
		{
			$this->db->where('id', $id);
			$this->db->update('user', $data);
			return array('status' => 200 , 'message' => 'Berhasil mengubah karyawan');
		}

		public function maxid()
		{
			$laastid = $this->db->query("SELECT MAX(id) as lastid FROM user ")->result();
			foreach($laastid as $id){
				$nextid = $id->lastid + 1;
			} 
			return $nextid;
		}


		public function searchByName($nama)
		{
			$res = $this->db->get_where('user',array('nama'=>$nama))->result();
			return $res;
		}

	}
?>