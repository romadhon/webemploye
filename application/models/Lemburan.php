<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Lemburan extends CI_Model {

  public function getLembur($id, $bln){//data bulan ini

    $tahun = date("Y");
    $tanggal = $this->Reff_data->getDateDimension($tahun, $bln);
    $absensi = $this->AbsensiModel->getPresensiPerPegawai($id);

    return array('status' => 200 , 'message' => 'Berhasil mengambil data lembur', 'absensi' => $absensi, 'tanggal' => $tanggal);
    
  }
  
  public function lemburanPegawai($id,$m)
  {
      $header = $this->UserModel->getEmployeePerId($id);
      $data['header'] = $header;
      $res = $this->AbsensiModel->getPresensiPerPegawai($id);
      $data['heading'] = $res;
      $dimension = $this->Reff_data->getDateDimension(date("Y"),$m);
      $data['date_dimension'] = $dimension;
      $this->load->view('presensiperpegawai',$data);
  }

}