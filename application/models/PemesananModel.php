<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class PemesananModel extends CI_Model {
		public function getAllPemesanan(){
			$this->db->select('*');
			$this->db->from('pemesanan');
			$res = $this->db->get()->result();

			if (count($res)==0) {
				return array('status' => 201 , 'message' => 'Tidak ada pemesanan.' );	
			}else{
				return array('status' => 200 , 'data' => $res);	
			}		
		}

		public function insertPemesanan($data)
		{
			$this->db->trans_start();
			$in=$this->db->insert('pemesanan',$data);
			if ($in) {
				
				if ($this->db->trans_status() === FALSE) {
						$this->db->trans_rollback();
						return array('status' => 500 , 'message' => 'Terjadi kesalahan server.' );	
					}else{
						$this->db->trans_commit();
						return array('status' => 200 , 'message' => 'Pemesanan berhasil.');	
					}
			}else{
				return array('status' => 201 , 'message' => 'Pemesanan gagal.');	
			}
		}

		public function batalPemesanan($data)
		{
			$this->db->trans_start();
			$this->db->where('id_pemesanan',$data['id_pemesanan']);
			$in=$this->db->update('pemesanan',$data);
			if ($in) {
				
				if ($this->db->trans_status() === FALSE) {
						$this->db->trans_rollback();
						return array('status' => 500 , 'message' => 'Terjadi kesalahan server.' );	
					}else{
						$this->db->trans_commit();
						return array('status' => 200 , 'message' => 'Pemesanan dibatalkan.');	
					}
			}else{
				return array('status' => 201 , 'message' => 'Gagal membatalkan pemesanan.');	
			}
		}

	}
?>