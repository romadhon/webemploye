<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class Reff_data extends CI_Model {

  public function getDateDimension($y,$m){
    return $this->db->get_where('time_dimension', array('year'=>$y, 'month'=>$m))->result(); 
  }

  public function getDateDimensionCustom($data){
    return $this->db->query("SELECT * FROM time_dimension where db_date between '".$data[tglmulai]."' and '".$data['tglakhir']."'")->result(); 
  }
  
  public function getRegions()
  {
    return $this->db->query("select * from bagian")->result();
  }

  public function getPositions()
  {
    return $this->db->get('ref_jabatan')->result();
  }
  
  function shift($data) {
      if ($data['action'] == ACT_VIEW) {
         $res = $this->db->get('ref_shift')->result(); 
         return $res;
        }elseif ($data['action'] == ACT_EDIT) {
            $this->db->trans_start();
            $this->db->update();
            if($this->db->trans_status() === true){
                $this->db->trans_commit();
                return 'sukses';
            } else {
               $this->db->trans_rollback(); 
            }
        }elseif ($data['action'] == ACT_DELETE) {
            
        }elseif ($data['action'] == ACT_INSERT) {
            $this->db->trans_start();
            $this->db->insert('ref_shift',array('nama' => $data['nama']));
            if($this->db->trans_status() === true){
                $this->db->trans_commit();
                return 'sukses';
            } else {
               $this->db->trans_rollback(); 
            }
        }
  }

  public function bagianById($id)
  {
      $res = $this->db->get_where('bagian', array('idbagian' => $id))->result();
      return $res;

  }
  function bagian($data) {
    if ($data['action'] == ACT_VIEW) {
       $res = $this->db->get('bagian')->result(); 
       return $res;
      }elseif ($data['action'] == ACT_EDIT) {
          $this->db->trans_start();
            $this->db->set('nama_bagian', $data['nama']);
            $this->db->where('idbagian', $data['idbagian']);
            $this->db->update('bagian');
          if($this->db->trans_status() === true){
              $this->db->trans_commit();
              return $this->db->get('bagian')->result();
          } else {
             $this->db->trans_rollback(); 
          }
      }elseif ($data['action'] == ACT_DELETE) {

        $this->db->trans_start();
        $this->db->delete('bagian', array('idbagian' => $data['idbagian'])); 
        if($this->db->trans_status() === true){
            $this->db->trans_commit();
            return $this->db->get('bagian')->result();
        } else {
           $this->db->trans_rollback(); 
        }

      }elseif ($data['action'] == ACT_INSERT) {
          $lastid = $this->db->query("SELECT MAX(idbagian) as lastid
          FROM bagian;")->result();

          foreach($lastid as $value) {
            $nextid = $value->lastid+100; 
          }
          
          $this->db->trans_start();
          $this->db->insert('bagian',array('idbagian' => $nextid,'nama_bagian' => $data['nama']));
          if($this->db->trans_status() === true){
              $this->db->trans_commit();
              return $this->db->get('bagian')->result();
          } else {
             $this->db->trans_rollback(); 
          }
      }
}
}