<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AuthModel extends CI_Model {
	var $client_service = "bagicode-client";
	var $auth_key = "restfull";

	public function check_auth_client(){
		$input_client_service = $this->input->get_request_header('Client_Service', TRUE);
		$input_auth_key = $this->input->get_request_header('Auth_Key', TRUE);

		if ($input_client_service == $this->client_service && $input_auth_key == $this->auth_key) {
			return true;
		}else{
			return json_output(401, array('status' => 401, 'message' => 'Unauthorized Headers.'));
		}
	} 	

}
?>