<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class RegistrationModel extends CI_Model {

		public function registrasi($username, $password, $nama, $ktp, $alamat, $hp, $email){

			$query = $this->db->query("select * from registration where ktp = '".$ktp."'");
			$res = $query->result_array();

			foreach ($res as $key) {
				$id = $key['ktp'];
			}

			$query = $this->db->query("select * from users where username = '".$username."'");
			$res = $query->result_array();

			foreach ($res as $key) {
				$user = $key['username'];
			}

			if (hash_equals($ktp, $id)) {
					return array('status' => 201 , 'message' => 'Duplicate ID Card.' );				
			}else if(hash_equals($username,$user)) {
					return array('status' => 201 , 'message' => 'Username Already Used.' );				
			}else{
				$this->db->trans_start();
				$this->db->insert('registration', array('ktp' => $ktp, 'nama' => $nama, 'alamat' => $alamat, 'hp' => $hp, 'email' => $email ));
				$this->db->insert('users', array('id_user'=>$ktp,'username' => $username, 'password' => md5($password), 'last_login' => ''));
				$this->db->insert('profile', array('id_profile' => $ktp, 'nama' =>$nama, 'alamat' => $alamat, 'hp' =>$hp, 'email' => $email, 'img' =>''));
				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
					return array('status' => 500 , 'message' => 'Internal Server Error' );
				}else{
					$this->db->trans_commit();
					return array('status' => 200 , 'message' => 'Registration Success.');
				}
	
			}
		}

	}

?>