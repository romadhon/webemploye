<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
class AbsensiModel extends CI_Model {
  public function view(){
    return $this->db->get('pegawai')->result(); 
  }
  
  public function getPresensi(){
    return $this->db->get('presensi')->result(); 
  }

 public function getPresensiPerPegawai($id){

$data = $this->db->get_where('presensi',array('idpegawai'=>$id))->result();
  return $data;
  }
  public function upload_file($filename){
     // Load librari upload
    $config['upload_path'] = './file/';
    $config['allowed_types'] = 'xlsx';
    $config['max_size']  = '2048';
    $config['overwrite'] = true;
    $config['file_name'] = $filename;

    $this->load->library('upload',$config);
    if($this->upload->do_upload('file')){ 
      // Jika berhasil :
      $return = array('result' => 'success', 'file' => $this->upload->data(), 'error' => '');
      return $return;
    }else{
      // Jika gagal :
      $return = array('result' => 'failed', 'file' => '', 'error' => $this->upload->display_errors());
      return $return;
    }
  }
  
  function finalPresensi() {
     
      $presensi = $this->db->query("SELECT * FROM `presensi` where id = '35006' ORDER BY `idpegawai`, `tgl`, `waktu`")->result();
      $idpegawai = [];
      $tanggal = [];
      $waktu = [];
       
      foreach ($presensi as $value) {
          $idpegawai[] = $value->idpegawai;
          $tanggal[] = $value->tgl; 
          $waktu[] = $value->waktu; 
       }
       $tempid = $idpegawai[0];
       $temptgl = $tanggal[0];
      foreach ($presensi as $key => $value) {
          $tempWaktu = [];//1,1,1,1 2
          
          if ($tempid == $value->idpegawai) {
              if ($temptgl == $value->tgl) {
                 $tempWaktu[] = $value->waktu;
                }
          } else {
                  
              if ($tempWaktu[0]==null){
                  $waktu1 = null;
              }else{
                  $waktu1 = $tempWaktu[0];
              }
              
                 if ($tempWaktu[1]==null){
                  $waktu2 = null;
              }else{
                  $waktu2 = $tempWaktu[1];
              }
              
                 if ($tempWaktu[2]==null){
                  $waktu3 = null;
              }else{
                  $waktu3 = $tempWaktu[2];
              }
              
                 if ($tempWaktu[3]==null){
                  $waktu4 = null;
              }else{
                  $waktu4 = $tempWaktu[3];
              }
              
                  $data = array('idpegawai'=>$value->idpegawai,
                      'tgl'=>$value->tgl,
                      'waktu1'=>$waktu1,
                      'waktu2'=>$waktu2,
                      'waktu3'=>$waktu3,
                      'waktu4'=>$waktu4);
                  
                  $this->db->trans_start();

                    $this->db->insert('final_presensi', $data);
                    if ($this->db->trans_status === true){
                       $this->db->trans_commit(); 
                    }else{
                         $this->db->trans_rollback(); 
                    }
                       
                    
          }
          
           if ($tempid != $value->idpegawai) {
             $tempid = $value->idpegawai;
         }
         
          if ($temptgl != $value->tgl) {
             $temptgl = $value->tgl;
         }
      }

  }

  public function lemburanPerPegawai($data)
  {
    $res = $this->db->query("SELECT * FROM presensi where idpegawai = '".$data['idpegawai']."' 
    and tgl between '".$data['tglmulai']."' and '".$data['tglakhir']."'")->result();
    return $res;
  }

}