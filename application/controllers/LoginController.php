<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class LoginController extends CI_Controller {

	public function index()
	{
		redirect(base_url());
		// $response = $this->db->get('employee')->result();
		// $this->load->view('login',$response);
	}

	function aksi_login(){ //login web
		
		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$where = array(
			'id' => $username,
			'password' => md5($password)
			);

		$res = $this->LoginModel->cek_login("user",$where)->result_array();
		
		if(count($res) > 0){
 
		 foreach ($res as $key) {
		 	$role = $key['role'];
		 	$nama = $key['nama'];
		 }

			$data_session = array(
				'nama' => $nama,
				'role' => $role,
				'status' => "login"
				);
 
				if ($role == ROLE_ADMIN || $role == ROLE_PENGAWAS) {
					$this->session->set_userdata($data_session);
					redirect(base_url("/Dashboard/home"));
				} else {
					echo "<script>
					alert('Anda tidak ada akses masuk!');
					window.location.href='index';  
					</script>";
				}
			
 
		}else{
			echo "<script>
					alert('Username dan password salah !');
					window.location.href='index';  
					</script>";
		}
	}

	public function mobile_login()
	{
		
		$username = $this->input->post('username');
		$password = $this->input->post('password');
		$response = $this->UserModel->mobile_login($username, $password);
		json_output($response['status'], $response);
	}

 
	function logout(){
		$this->session->sess_destroy();
		redirect(base_url('login'));
	}
}
