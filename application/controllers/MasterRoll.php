<?php
defined('BASEPATH') OR exit('No direct script access allowed');
define('KELOMPOK1', 1);
class MasterRoll extends CI_Controller {
  
    public function __construct() {
        parent::__construct();
    }

    public function update_roll() {
        $kelompok1 = $this->db->query("SELECT DISTINCT shift FROM master_roll WHERE kelompok = 1")->result_array();
        foreach($kelompok1 as $row1){
            $shift = $row1['shift'];    
        }

        if ($shift=='Malam') {
            $this->db->query("UPDATE master_roll SET shift = 'Siang' WHERE kelompok = 1");
        } else {
            $this->db->query("UPDATE master_roll SET shift = 'Malam' WHERE kelompok = 1");
        }
        
        $kelompok2 = $this->db->query("SELECT DISTINCT shift FROM master_roll WHERE kelompok = 2")->result_array();
        foreach($kelompok2 as $row2){
            $shift = $row2['shift'];    
        }

        if ($shift=='Siang') {
            $this->db->query("UPDATE master_roll SET shift = 'Malam' WHERE kelompok = 2");
        } else {
            $this->db->query("UPDATE master_roll SET shift = 'Siang' WHERE kelompok = 2");
        }
    }
    
    public function cekShift($id)
    {
        $res = $this->db->get_where('master_roll',array('id'=>$id))->result;
        foreach($res as $value){
            return $value->shift;
        }
    }
}
