<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Referensi extends CI_Controller {

	public function getDate()
	{
        $tahun = date("Y");
        $tanggal = date("m");
        $data = $this->Reff_data->getDateDimension($tahun, $tanggal);
		return $data;
	}
        
        public function data_shift() {
            $data = array('action'=> ACT_VIEW);
                $res['data'] = $this->Reff_data->shift($data);
                return $this->load->view('ref_shift',$res);
        }

        public function shift() {
            $action = $this->input->post('action');
            $name = $this->input->post('name');
            if ($action == ACT_EDIT) {
                $data = array('action'=> $action);
                return $this->Reff_data->shift($data);
            }elseif ($action == ACT_DELETE) {
                $data = array('action'=> $action);
                return $this->Reff_data->shift($data);
            }elseif ($action == ACT_INSERT) {
                $data = array('action'=> $action, 'name' => $name);
                return $this->Reff_data->shift($data);
            }
        }

        public function data_bagian() {
            $data = array('action'=> ACT_VIEW);
                $res['data'] = $this->Reff_data->bagian($data);
                return $this->load->view('ref_bagian',$res);
        }

        public function tambah_bagian()
        {
            $action = $this->input->post('action');
            if ($action === NULL ){
                return $this->load->view('tambahbagian');
            } elseif ($action == ACT_INSERT) {
                $namabagian = $action = $this->input->post('nama');
                $data = array('action'=> ACT_INSERT,
                                'nama'=>$namabagian);
                $this->Reff_data->bagian($data);
                redirect(base_url("/Referensi/data_bagian"));
            }
        }

        public function ubah_bagian($id)
        {
            $action = $this->input->post('action');
            if ($action === NULL ){
                $res['data'] = $this->Reff_data->bagianById($id);
                return $this->load->view('ubahbagian',$res);
            } elseif ($action == ACT_EDIT) {
                $namabagian = $action = $this->input->post('nama');
                $idbagian = $action = $this->input->post('idbagian');
                $data = array('action'=> ACT_EDIT,
                                'idbagian' => $idbagian,
                                'nama'=>$namabagian);
                $this->Reff_data->bagian($data);
                redirect(base_url("/Referensi/data_bagian"));
            }
        }

        public function hapus_bagian($id)
        {
            $data = array('action'=> ACT_DELETE,
                                'idbagian'=>$id);
            $res['data'] = $this->Reff_data->bagian($data);
            return $this->load->view('ref_bagian',$res);
        }

        public function bagian() {
            $action = $this->input->post('action');
            // $name = $this->input->post('name');
            if ($action == ACT_EDIT) {
                $data = array('action'=> $action);
                return $this->Reff_data->shift($data);
            }elseif ($action == ACT_DELETE) {
                $data = array('action'=> $action);
                return $this->Reff_data->shift($data);
            }elseif ($action == ACT_INSERT) {
                $this->data_bagian();
            }
        }

}
