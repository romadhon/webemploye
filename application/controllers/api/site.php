<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class site extends CI_Controller {

	public function login()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'POST') {
			json_output(400, array('status' => 400,
						'message' => 'BAD REQUEST!!!'));
		}else{
			$check_auth_client = $this->AuthModel->check_auth_client();
			if ($check_auth_client == true) {
				$params = json_decode(file_get_contents('php://input'), TRUE);
				$username = $params['id'];
				$password = $params['password'];

				$response = $this->LoginModel->login($username, $password);
				json_output($response['status'], $response);
			}
		}
	}
 
}
