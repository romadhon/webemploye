<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	function __construct() {
        parent::__construct();
		session_start();
    }

	public function index()
	{
		$this->load->view('login');
	}

	function proses() {
		 
            $tgl_login=date("Y-m-d"); 
			$jam_login=date("H:i:s");
            $usr = $this->input->post('username');
            $psw = $this->input->post('password');
            $response = $this->UserModel->login_web($usr,$psw);
            if ($response['status']=='205') {
            	echo $response['message'];
            }else{
            	$data['a']=$response;
            	$this->load->view('welcome_message',$data);
            }
          
        
    }

    function logout() {
        $this->session->sess_destroy();
        redirect('login');
    }

}
