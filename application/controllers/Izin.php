<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Izin extends CI_Controller {
  private $filename = "import_data"; 
  
  public function __construct(){
    parent::__construct();
     $this->load->model('IzinModel');
	}

	public function index()
	{
		$res = $this->IzinModel->getAll();
		$data['heading'] = $res;
		$this->load->view('izin',$data);
	}

	public function detailPerizinan($id)
	{
		$res = $this->IzinModel->getDataIzinPeridijin($id);
		$data['heading'] = $res;
		$this->load->view('detail_izin',$data);
	}

	public function getAll()
	{
		$response = $this->IzinModel->getAll();
		json_output($response['status'], $response);
	}

	public function pengajuan()
	{
		$id = $this->input->post('idpegawai');
		$tglPengajuan = $this->input->post('tglPengajuan');
		$mulai = $this->input->post('mulai');
		$selesai = $this->input->post('selesai');
		$keterangan = $this->input->post('keterangan');
		$tipe = $this->input->post('tipe');
		$response = $this->IzinModel->ajukan($id, $tglPengajuan,$mulai,$selesai,$keterangan,$tipe);
		json_output($response['status'], $response);
	}

	public function getPerPegawai()
	{
		$id = $this->input->post('idpegawai');	
		$response = $this->IzinModel->getDataIzinPerid($id);
		json_output($response['status'], $response);
	}

	public function validasi()
	{
		$role = $this->session->userdata('role');
		$type = $this->input->post('type');
		$idijin = $this->input->post('idijin');
		$response = $this->IzinModel->validasi($role, $type, $idijin);
		$this->index();
	}
}
