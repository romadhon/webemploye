<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AbsensiController extends CI_Controller {
  private $filename = "import_data"; 
  
  public function __construct(){
    parent::__construct();
    
    $this->load->model('AbsensiModel');
    $this->load->library(array('PHPExcel','PHPExcel/IOFactory'));
}


	public function index()
	{
		$res = $this->AbsensiModel->getPresensi();
		$data['heading'] = $res;
		$this->load->view('absensi',$data);
    }
    
    public function search()
	{
		$nama = $this->input->post('nama');
		$res = $this->UserModel->searchByName($nama);
		$data['heading'] = $res;
		$this->load->view('absensi',$data);
	}

    public function upload_absensi()
    {
        $this->load->view('upload_absensi');
    }

	public function absensi()
	{
		$data = $this->AbsensiModel->getPresensi();
		return $data;
	}

  public function getEmployee()
  {
    $res = $this->UserModel->getEmployee();
    $data['heading'] = $res;
    $this->load->view('absensi',$data);
  }

    public function uploadTxt()
    {
        $fileName = $_FILES['file']['name'];
        $config['upload_path'] = './file/'; 
        $config['file_name'] = $fileName;
        $config['allowed_types'] = 'xls|xlsx|csv|txt|cm';
        $config['max_size'] = 10000;
         
        $this->load->library('upload');
        $this->upload->initialize($config);

        unlink("./file/".$fileName);
         
        if(! $this->upload->do_upload('file') )
        $this->upload->display_errors();
             
        $media = $this->upload->data('file');
        $inputFileName = './file/'.$fileName;

          $handle = fopen($inputFileName, "r");
            $idpegawai = [];
            if ($handle) {
                while (($line = fgets($handle)) !== false) {

                    $lineArr = explode(" ", "$line");
                    // instead assigning one by onb use php list -> http://php.net/manual/en/function.list.php
                    list($emp_id, $date_data) = $lineArr;
                    $id_finger = substr($emp_id,0,5);
                    $tgl = substr($emp_id,5,strlen($emp_id));

                    $hour = substr($date_data,0,5);
                    $second = substr($date_data,5,2);
                    $time = $hour.':'.$second;

                    $idpegawai = $id_finger;
                    $res_shift = $this->db->get_where('master_shift',array('tanggal'=>$tgl))->result();
                    foreach ($res_shift as $value) {
                        $pagi = $value->pagi;//1
                        $malam = $value->malam;//3
                    }

                    $user = $this->db->get_where('user', array('id'=>$id_finger))->result();
                    foreach ($user as $v_user){ 
                        $u_kelompok = $v_user->kelompok;
                    }

                    if($pagi == $u_kelompok){
                        $shift = 'Pagi';
                    } else if($malam == $u_kelompok){
                        $shift = 'Malam';
                    } else {
                        $shift = 'Pagi';
                    }
                    
                    $data = array(
                    "idpegawai"=> $id_finger,
                    "tgl"=> $tgl,
                    "waktu"=> $time,
                    "shift"=>$shift,
                    "status"=>'H'
                    );

                    //Insert data
                    $this->db->trans_start();

                    $this->db->insert('presensi', $data);
                    $this->db->trans_commit();

                }
                fclose($handle);
                $res = $this->AbsensiModel->getPresensi();
                $data['heading'] = 'Upload Berhasil';
                $this->load->view('upload_absensi',$data);
            } else {
                $res = $this->AbsensiModel->getPresensi();
                $data['heading'] = 'Upload Gagal';
                $this->load->view('upload_absensi',$data);
            }
        
    }
    
  function finalPresensi() {
      $this->AbsensiModel->finalPresensi();
      
      return 'sukses';
  }

/*

untuk menampilkan presensi pegawai
*/

    public function karyawan($id,$m)
    {
        $tahun = $this->input->post('tahun');
        $data_session = array(
            'id_karyawan' => $id,
            'tahun' => $tahun
            );
     
          $this->session->set_userdata($data_session);

        $header = $this->UserModel->getEmployeePerId($id);
        $data['pegawai'] = $header;  
        $res = $this->AbsensiModel->getPresensiPerPegawai($id);
        $dimension = $this->Reff_data->getDateDimension($this->session->userdata('tahun'),$m); 
             
        $data['presensi'] =$this->presensiMapper($dimension,$res);
        $this->load->view('presensiperpegawai',$data);
    }

    public function mobileKaryawan($id,$m)
    {
        $tahun = $this->input->post('tahun');
        $header = $this->UserModel->getEmployeePerId($id);
        $res = $this->AbsensiModel->getPresensiPerPegawai($id);
        $dimension = $this->Reff_data->getDateDimension($tahun,$m);

        $data['pegawai'] = $header; 
        $data['presensi'] =$this->presensiMapper($dimension,$res);
        json_output($response['status'], $data);
    }

    public function lemburanPerPegawai()
    {
        $idpegawai = $this->input->post('idpegawai');
        $tglmulai = $this->input->post('tglmulai');
        $tglakhir = $this->input->post('tglakhir');
        $tahun = $this->input->post('tahun');
        $bulan = $this->input->post('bulan');
        $params = array('idpegawai' => $idpegawai,
        'tglmulai' => $tglmulai,
        'tglakhir' => $tglakhir);
        $res = $this->AbsensiModel->lemburanPerPegawai($params);
        $dimension = $this->Reff_data->getDateDimensionCustom($params);
        $data['presensi'] =$this->presensiMapper($dimension,$res);
        $data['scores'] = $this->scores($data);
        $data['lemburan'] = $this->lemburan($data);
        json_output($response['status'], $data);
    }

    public function scores($data)
    {
        $tanggal = [];
        $scores = [];
        foreach ($data['presensi'] as $key => $value) {
            if ($value['status'] == 'A') {
                $tanggal[] = $value['tanggal'];
            }
        }
        for ($i=0; $i < count($tanggal);$i++) {
            $tmpTanggal = $tanggal[$i];
            for ($j=0; $j < 3; $j++) { 
                $scores[] = date('Y-m-d', strtotime($tmpTanggal .' +1 day'));
                $tmpTanggal = date('Y-m-d', strtotime($tmpTanggal .' +1 day'));
            }
        }
        
       return array_values(array_unique($scores));
    }

    public function lemburan($data)
    {
        $presensi = [];
        $scores = $data['scores'];
        $lemburan =[];
        foreach ($data['presensi'] as $key => $value) {
            $presensi[] = $value['tanggal'];
        }
        for ($i=0; $i < count($presensi); $i++) { 
            for ($j=0; $j < count($scores); $j++) { 
                
                if ($scores[$j] != $presensi[$i]) {
                    $lemburan[] = $presensi[$i];
                } 
            }
        }
//        print_r($presensi);
//        print_r($data['scores']);
//        print_r(array_values(array_unique($lemburan)));die;
    }

    public function presensiMapper($calender, $res)
    {
        $presensi = [];

        foreach($calender as $vDate){
            $tgl = $vDate->db_date;
            $shift='';
            $waktu_masuk='';
            $waktu_istirahat='';
            $waktu_istirahat_masuk='';
            $waktu_pulang='';
            $status ='A';
            $lembur=0;

            $masukmalam = [];
            foreach ($res as $value) {
              if ($value->shift == SHIFT_MALAM) {
                if ($value->waktu > SIANG_PULANG && $value->waktu < MALAM_MASUK ) {
                  $next_date = date('Y-m-d', strtotime($value->tgl .' +1 day'));
                  $masukmalam[] = $next_date.' '.$value->waktu;
                }
              }
            }
            

            $istirahat = [];
            foreach($res as $vPresensi){
                if ($vDate->db_date == $vPresensi->tgl) {
                    
                        // jam masuk
                    if ($vPresensi->shift == SHIFT_PAGI) {
                        $shift = $vPresensi->shift;
                        if ($vPresensi->waktu < SIANG_MASUK) {
                            $waktu_masuk = $vPresensi->waktu;
                        }
                        // jam istirahat
                        if ($vPresensi->waktu > SIANG_ISTIRAHAT_KELUAR && $vPresensi->waktu < SIANG_ISTIRAHAT_MASUK ) {
                            $istirahat[]=$vPresensi->waktu;
                         }
                        // jam pulang
                         if ($vPresensi->waktu > SIANG_PULANG) {
                            $waktu_pulang = $vPresensi->waktu;
                         }
                         // status kehadiran
                           $status = 'H';
                         // lembur
                         if ($vPresensi->waktu > SIANG_PULANG) {
                            if ($vPresensi->waktu > BATAS_SIANG_LEMBUR3) {
                                $lembur = 3;
                            }elseif ($vPresensi->waktu > BATAS_SIANG_LEMBUR2 && $vPresensi->waktu < BATAS_SIANG_LEMBUR3) {
                                $lembur = 2;
                            }elseif ($vPresensi->waktu > BATAS_SIANG_LEMBUR1 && $vPresensi->waktu < BATAS_SIANG_LEMBUR2) {
                                $lembur = 1;
                            }
                          }
                    } elseif ($vPresensi->shift == SHIFT_MALAM) {

                        // jam masuk
                        for($i = 0; $i < count($masukmalam) ; $i++){
                            $tglMalam = explode(" ", $masukmalam[$i]);
                            if($vPresensi->tgl== $tglMalam[0]){
                                $waktu_masuk = $tglMalam[1];
                                $status = 'H';
                                $shift = $vPresensi->shift;
                            }
                          }

                        // jam istirahat
                        if ($vPresensi->waktu > MALAM_ISTIRAHAT_KELUAR && $vPresensi->waktu < MALAM_ISTIRAHAT_MASUK ) {
                            $istirahat[]=$vPresensi->waktu;
                         }
                        // jam pulang
                         if ($vPresensi->waktu > MALAM_PULANG && $vPresensi->waktu < BATAS_MALAM_LEMBUR) {
                            $waktu_pulang = $vPresensi->waktu;
                            $status = 'H';
                            $shift = $vPresensi->shift;

                            // lembur
                            if ($vPresensi->waktu > BATAS_MALAM_LEMBUR3) {
                                $lembur = 3;
                            }elseif ($vPresensi->waktu > BATAS_MALAM_LEMBUR2 && $vPresensi->waktu < BATAS_MALAM_LEMBUR3) {
                                $lembur = 2;
                            }elseif ($vPresensi->waktu > BATAS_MALAM_LEMBUR1 && $vPresensi->waktu < BATAS_MALAM_LEMBUR2) {
                                $lembur = 1;
                            }
                         }
                    
                    }
                      
                }
            }
            $presensi[] = array(
                'tanggal' => $tgl,
                'shift' => $shift,
                'masuk' => $waktu_masuk,
                'istirahat1' => $istirahat[0],
                'istirahat2' => $istirahat[1],
                'pulang' => $waktu_pulang,
                'status' => $status,
                'lembur' => $lembur);
        }

        // print_r($presensi);die;
        return $presensi;
    }

    public function lemburanMapper($calender, $scores)
    {
        $lemburan = [];

        foreach($calender as $vDate){
            $tanggal = $vDate->db_date;
            foreach($scores as $vScores){
                if ($vDate->db_date == $vScores->tgl) {

                }
            }
        }
    }

}
