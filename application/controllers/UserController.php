<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class UserController extends CI_Controller {
	//mengambil data profile user
	public function get(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'POST') {
			json_output(400, array('status' => 400,
						'message' => 'BAD REQUEST!!!'));
		}else{
			
				$params = json_decode(file_get_contents('php://input'), TRUE);
				$ktp = $params['id_user'];

				$response = $this->UserModel->getProfile($ktp);
				json_output($response['status'], $response);
			
		}
	}

public function next()
{
	$this->load->view('welcome_message');
}
	//update data profile
	public function update_profile($id){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'POST') {
			json_output(400, array('status' => 400,
						'message' => 'BAD REQUEST!!!'));
		}else{
			
				$params = json_decode(file_get_contents('php://input'), TRUE);
				$response = $this->UserModel->updateProfile($id, $params);
				json_output($response['status'], $response);
			
		}
	}

	public function registrasi(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'POST') {
			json_output(400, array('status' => 400,
						'message' => 'BAD REQUEST!!!'));
		}else{
			
				$params = json_decode(file_get_contents('php://input'), TRUE);
				$response = $this->UserModel->userRegistrasi($params);
				json_output($response['status'], $response);
			
		}	
	}

	public function userLogin()
	{
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'POST') {
			json_output(400, array('status' => 400,
						'message' => 'BAD REQUEST!!!'));
		}else{
				$params = json_decode(file_get_contents('php://input'), TRUE);
				$response = $this->UserModel->login($params);
				json_output($response['status'], $response);
		}	
	}

	public function getKodeRegistrasi()
	{

		$response = $this->UserModel->send_verification_email();
		json_output($response['status'],$response);
	}

}

?>

