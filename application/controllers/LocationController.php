<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class LocationController extends CI_Controller {
		public function getAllLocation(){
			$method = $_SERVER['REQUEST_METHOD'];
			if ($method != 'POST') {
				json_output(400, array('status' => 400,
							'message' => 'BAD REQUEST!!!'));
			}else{
				$response = $this->LocationModel->getAllLocation();
				json_output($response['status'], $response);
			}
		}
	}

?>