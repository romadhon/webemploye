<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class PemesananController extends CI_Controller {
		public function getAllPemesanan(){
			$method = $_SERVER['REQUEST_METHOD'];
			if ($method != 'POST') {
				json_output(400, array('status' => 400,
							'message' => 'BAD REQUEST!!!'));
			}else{
				$response = $this->PemesananModel->getAllPemesanan();
				json_output($response['status'], $response);
			}
		}

		public function orderHelp(){
				$method = $_SERVER['REQUEST_METHOD'];
				if ($method != 'POST') {
					json_output(400, array('status' => 400,
								'message' => 'BAD REQUEST!!!'));
				}else{
					$params = json_decode(file_get_contents('php://input'), TRUE);
					$response = $this->PemesananModel->insertPemesanan($params);
					json_output($response['status'], $response);
				}
		}

		public function orderCancel(){
				$method = $_SERVER['REQUEST_METHOD'];
				if ($method != 'POST') {
					json_output(400, array('status' => 400,
								'message' => 'BAD REQUEST!!!'));
				}else{
					$params = json_decode(file_get_contents('php://input'), TRUE);
					$response = $this->PemesananModel->batalPemesanan($params);
					json_output($response['status'], $response);
				}
		}

	}
		
?>