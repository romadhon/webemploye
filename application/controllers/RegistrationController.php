<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class RegistrationController extends CI_Controller {

	public function index()
	{
		
	}

	public function registrasi(){
		$method = $_SERVER['REQUEST_METHOD'];
		if ($method != 'POST') {
			json_output(400, array('status' => 400,
						'message' => 'BAD REQUEST!!!'));
		}else{
			$check_auth_client = $this->AuthModel->check_auth_client();
			if ($check_auth_client == true) {
				$params = json_decode(file_get_contents('php://input'), TRUE);
				$username = $params['username'];
				$password = $params['password'];
				$nama = $params['nama'];
				$ktp = $params['ktp'];
				$alamat = $params['alamat'];
				$hp = $params['hp'];
				$email = $params['email'];

				$response = $this->RegistrationModel->registrasi($username, $password, $nama, $ktp, $alamat, $hp, $email);
				json_output($response['status'], $response);
			}
		}
	}
}