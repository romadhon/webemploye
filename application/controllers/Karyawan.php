<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Karyawan extends CI_Controller {
  public function __construct(){
    parent::__construct();
    $this->load->model('Lemburan');
}

	public function index()
	{
		$data['heading'] = $this->AbsensiModel->view();
		$this->load->view('lembur',$data);
	}

	public function data_karyawan()
	{
		$res = $this->UserModel->getEmployee();
		$data['heading'] = $res;

		$jumlah_data = $this->UserModel->jumlah_data();
		
		$this->load->library('pagination');

		$config['base_url'] = base_url().'karyawan/data_karyawan/';
		$config['total_rows'] = $jumlah_data;
		$config['per_page'] = 10;
		$config['query_string_segment'] = 'start';
 
$config['full_tag_open'] = '<div class="pagination" style="margin-top:0px">';
$config['full_tag_close'] = '</div>';
 
$config['first_link'] = 'First';
$config['first_tag_open'] = '<div>';
$config['first_tag_close'] = '</div>';
 
$config['last_link'] = 'Last';
$config['last_tag_open'] = '<div>';
$config['last_tag_close'] = '</div>';
 
$config['next_link'] = 'Next';
$config['next_tag_open'] = '<div>';
$config['next_tag_close'] = '</div>';
 
$config['prev_link'] = 'Prev';
$config['prev_tag_open'] = '<div>';
$config['prev_tag_close'] = '</div>';
 
$config['cur_tag_open'] = '<div class="active"><a>';
$config['cur_tag_close'] = '</a></div>';
 
$config['num_tag_open'] = '<div>';
$config['num_tag_close'] = '</div>';

		$from = $this->uri->segment(3);
			
		$this->pagination->initialize($config);		
		$data['heading'] = $this->UserModel->data($config['per_page'],$from);
		
		$this->load->view('data_karyawan',$data);
	}

	public function detail($id)
	{
			$header = $this->UserModel->getEmployeePerId($id);
			$data['header'] = $header;
			$this->load->view('detailkaryawan',$data);
	}

	public function hapus()
	{
			$id = $this->input->post('id');
			$this->UserModel->hapus($id);
			$this->data_karyawan();
	}

	public function ubah()
	{
		$action = $this->input->post('action');
		if ($action=='get') {
			$id = $this->input->post('id');
			$header = $this->UserModel->getEmployeePerId($id);
			$data['header'] = $header;
			$ref_region = $this->Reff_data->getRegions();
			$ref_positions = $this->Reff_data->getPositions();
			$data['ref_positions'] = $ref_positions;
			$data['ref_region'] = $ref_region;
			$this->load->view('ubahkaryawan',$data);
		} else {
			$id = $this->input->post('id');
			$nama = $this->input->post('nama');
			$alamat = $this->input->post('alamat');
			$kelompok = $this->input->post('kelompok');
			$bagian = $this->input->post('bagian');
			$jabatan = $this->input->post('jabatan');

			$data = array(
			'nama' => $nama,
			'alamat' => $alamat,
			'kelompok' => $kelompok,
			'jabatan' => $jabatan,
			'role' => $bagian);

			$this->UserModel->ubah($data,$id);
			$this->detail($id);
		}
		
	}

	public function tambah()
	{
		$action = $this->input->post('action');
			if ($action != "add"){
				$ref_region = $this->Reff_data->getRegions();
				$ref_positions = $this->Reff_data->getPositions();
				$data['ref_positions'] = $ref_positions;
				$data['ref_region'] = $ref_region;
				$this->load->view('tambahkaryawan',$data);
			} else {
				$nextid = $this->UserModel->maxid();
				$action = $this->input->post('action');
				$nama = $this->input->post('nama');
				$bagian = $this->input->post('bagian');
				$jabatan = $this->input->post('jabatan');
				$jenis_kelamin = $this->input->post('jenis_kelamin');
				$kelompok = $this->input->post('kelompok');
				$alamat = $this->input->post('alamat');

					$data = array(
						'id'=>$nextid,
						'id_karyawan'=>'KR'.$nextid,
						'nama'=>$nama,
						'role'=>$bagian,
						'jabatan'=>$jabatan,
						'jk'=>$jenis_kelamin,
						'password'=>md5('12345'),
						'kelompok'=>$kelompok,
						'alamat'=>$alamat,
						'keterangan'=>'');
	
						$this->db->insert('user',$data);
	
					$this->data_karyawan();
				
			}
			
	}

	public function search()
	{
		$nama = $this->input->post('nama');
		$res = $this->UserModel->searchByName($nama);
		$data['heading'] = $res;
		$this->load->view('data_karyawan',$data);
	}
}
