<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function home()
	{
		$data['nama'] = $this->session->userdata('nama');
		$data['role'] = $this->session->userdata('role');
		$data['jml_karyawan'] = $this->UserModel->jumlah_data();
		$this->load->view('dashboard',$data);
	}

	function logout(){
		$user_data = $this->session->all_userdata();
        foreach ($user_data as $key => $value) {
            if ($key != 'nama' && $key != 'role' && $key != 'status') {
                $this->session->unset_userdata($key);
            }
        }
		$this->session->sess_destroy();
		redirect(base_url());
		
		
	}
}
