<?php
defined('BASEPATH') OR exit('No direct script access allowed');

defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);


defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);


defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');


defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code


// ACTION

define('ACT_VIEW','view');
define('ACT_INSERT','insert');
define('ACT_EDIT','edit');
define('ACT_DELETE','delete');

//SHIFT
define('SHIFT_PAGI','Pagi');
define('SHIFT_SIANG','Siang');
define('SHIFT_MALAM','Malam');

//JADWAL KERJA SHIFT SIANG
define('SIANG_MASUK','07:00:00');
define('SIANG_ISTIRAHAT_KELUAR','12:00:00');
define('SIANG_ISTIRAHAT_MASUK','13:00:00');
define('SIANG_PULANG','16:00:00');
define('BATAS_SIANG_LEMBUR3','19:00:00');
define('BATAS_SIANG_LEMBUR2','18:00:00');
define('BATAS_SIANG_LEMBUR1','17:00:00');

//JADWAL KERJA SHIFT MALAM
define('MALAM_MASUK','19:00:00');
define('MALAM_ISTIRAHAT_KELUAR','00:00:00');
define('MALAM_ISTIRAHAT_MASUK','01:00:00');
define('MALAM_PULANG','04:00:00');
define('BATAS_MALAM_LEMBUR','08:00:00');
define('BATAS_MALAM_LEMBUR3','07:00:00');
define('BATAS_MALAM_LEMBUR2','06:00:00');
define('BATAS_MALAM_LEMBUR1','05:00:00');

// role
define('ROLE_ADMIN','Admin');
define('ROLE_PENGAWAS','Pengawas');